﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FluentCode.Scrawl.Sdk.Plugins.Nuget
{
    [Export(typeof(FluentCode.Scrawl.Sdk.CommandSearchPlugin))]
    public class CommandSearchPlugin : FluentCode.Scrawl.Sdk.CommandSearchPlugin
    {
        public CommandSearchPlugin()
        {
        }

        public override CommandSearchResults Search(CommandSearchQuery query)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            var resultTask = client.GetStringAsync("http://www.nuget.org/api/v2/Search()?$filter=IsLatestVersion&$skip=0&$top=30&searchTerm=%27" + query.Query + "%27&includePrerelease=false");
            resultTask.Wait();

            JObject result = JObject.Parse(resultTask.Result);
            IEnumerable<JToken> items = result["d"];

            var resultList = new List<NugetPackageMetadata>();
            foreach (var item in items)
            {
                var title = item["Title"].ToString();
                var id = item["Id"].ToString();
                resultList.Add(new NugetPackageMetadata
                {
                    Id = id,
                    Name = string.IsNullOrEmpty(title) ? id : title,
                    Description = item["Description"].ToString(),
                    Authors = new[] { item["Authors"].ToString() },
                    LicenseName = item["LicenseNames"].ToString(),
                    Version = item["Version"].ToString(),
                    IconUrl = item["IconUrl"].ToString(),
                    DownloadCount = Convert.ToInt32(item["DownloadCount"].ToString()).ToString("N0", CultureInfo.CurrentUICulture)
                });
            }

            return new CommandSearchResults
            {
                TemplatePath = this.PluginResourcePath("resultTemplate.xml"),
                Data = resultList
            };
        }

        public override PluginMetadata LoadMetadata()
        {
            return new PluginMetadata
            {
                Name = "NuGet",
                Description = "Package Manager",
                ImagePath = this.PluginResourcePath("nugetlogo.png"),
                Aliases = new[] { "nuget", "nupkg", "install" }
            };
        }
    }
}
