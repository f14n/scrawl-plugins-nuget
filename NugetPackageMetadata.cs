﻿namespace FluentCode.Scrawl.Sdk.Plugins.Nuget
{
    public class NugetPackageMetadata
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string LicenseName { get; set; }

        public string ProjectSite { get; set; }

        public string DownloadCount { get; set; }

        public string Version { get; set; }

        public string IconUrl { get; set; }

        public string[] Authors { get; set; }
    }
}